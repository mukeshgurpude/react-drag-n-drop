# Outliner❕
> Live at: https://outlines-1.netlify.app/

A reorderable outline list example with react.js.

## Features
- Adding new outlines
- Editing outlines
- Reordering
- Autosave order

## Built With
- [Vite](https://vitejs.dev/)
- [ReactJs](https://reactjs.org)
- [react-movable](https://react-movable.netlify.app/)
