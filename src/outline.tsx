import styles from './outline.module.css'

type Props = {
  id: number
  text: string,
  update_text: (id: number, text: string) => void
}

export default function Outline({ id, text, update_text }: Props) {
  return <div className={styles.card}>
    <textarea className={styles.input} value={text} onChange={(e) => update_text(id, e.target.value)}></textarea>
    <button className={styles.handle} data-movable-handle>Move</button>
  </div>
}
