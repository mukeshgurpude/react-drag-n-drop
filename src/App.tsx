import { useState } from "react"
import { arrayMove, List } from "react-movable"
import Outline from "./outline"
import Input from "./input"
import { get_outlines, get_sort_order, update_order, update_outlines } from "./utils"
import styles from './App.module.css'

function App() {
  const [outlines, setOutlines] = useState(get_outlines())
  const [order, setOrder] = useState(get_sort_order())

  function update_text(id: number, text: string) {
    const copied = outlines.slice()
    copied[id] = text
    setOutlines(update_outlines(copied))
  }

  function add_new_outline(text: string) {
    const copied = outlines.slice()
    copied.push(text)
    setOutlines(update_outlines(copied))
    setOrder(update_order([...order, copied.length - 1]))
  }

  return <>
    <h1>Outlines</h1>
    <List values={order}
      onChange={({ oldIndex, newIndex }) => setOrder(update_order(arrayMove(order, oldIndex, newIndex)))}
      renderList={({ children, props }) => <section {...props} className={styles.container} children={children} />}
      renderItem={({ props, value }) => <div {...props}>
        <Outline id={value} text={outlines[value]} update_text={update_text} />
      </div>}
    />
    <Input add_new={add_new_outline} />
  </>
}

export default App
