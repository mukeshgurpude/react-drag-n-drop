const outlineList = [
  'What is the difference between an economic crisis and a financial crisis?',
  'What exactly is an economic crisis?',
  'How did the current economic crisis come about?',
  'What are the potential consequences of an economic crisis?',
  'Is there anything that can be done to prevent an economic crisis?',
  'The Impact of the Economic Crisis on Global economies',
  'What Can Be Done to Prevent Another Economic Crisis?'
]
const LOCALSTORAGE_KEY = 'outlines'
const SORT_KEY = 'sort_order'

export function get_outlines(): Array<string> {
  const outlines = localStorage.getItem(LOCALSTORAGE_KEY)
  return outlines === null ? outlineList : JSON.parse(outlines)
}

export function get_sort_order(): Array<number> {
  const sort_order = localStorage.getItem(SORT_KEY)
  if (sort_order === null) {
    return Array.from({ length: get_outlines().length }, (_, i) => i)
  }
  return JSON.parse(sort_order)
}

export function update_outlines(outlines: Array<string>): Array<string> {
  localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(outlines))
  return outlines
}

export function update_order(order: Array<number>): Array<number> {
  localStorage.setItem(SORT_KEY, JSON.stringify(order))
  return order
}
