import { useState } from "react"
import styles from './App.module.css'

type Props = {
  add_new: (text: string) => void
}

export default function Input({ add_new }: Props) {
  const [value, setValue] = useState('')

  return <form onSubmit={(ev) => {
    ev.preventDefault()
    if (value === '') return
    add_new(value)
    setValue('')
  }} className={styles.form}>
    <input value={value} onChange={(e) => setValue(e.target.value)} placeholder='+ Add Outline' />
  </form>
}
